package server

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/zlyzol/dexarbot/db"
)

func (srv *Server) HandleBotStart(w http.ResponseWriter, r *http.Request) {
	srv.bot.Start()
	w.WriteHeader(http.StatusAccepted)
	fmt.Fprint(w, `{"response": "ok"}`)
}
func (srv *Server) HandleBotStop(w http.ResponseWriter, r *http.Request) {
	srv.bot.Stop()
	w.WriteHeader(http.StatusAccepted)
	fmt.Fprint(w, `{"response": "ok"}`)
}
func (srv *Server) HandleBotStatus(w http.ResponseWriter, r *http.Request) {
	status := srv.bot.GetStatus()
	fmt.Fprintf(w, `{"status": "%v"}`, status)
}
func (srv *Server) HandleBotStats(w http.ResponseWriter, r *http.Request) {
	stats, err := db.GetStats(srv.ctx)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"error": "%v"}`, appErrDataAccessFailure)
		return
	}
	stats.TimeRunning = time.Since(srv.bot.StartTime).String()

	if err := json.NewEncoder(w).Encode(stats); err != nil {
		log.Printf("ERROR: stats encoding failed: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"error": "%v"}`, appErrJsonCreationFailure)
		return
	}
}
func (srv *Server) HandleBotLogs(w http.ResponseWriter, r *http.Request) {
	limit, err := strconv.ParseUint(chi.URLParam(r, "limit"), 0, 64)
	if err != nil || limit == 0 {
		limit = 256
	}
	logs, err := db.GetLogs(srv.ctx, limit)
	if err := json.NewEncoder(w).Encode(logs); err != nil {
		log.Printf("ERROR: logs encoding failed: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"error": "%v"}`, appErrJsonCreationFailure)
		return
	}
}

func (srv *Server) HandleBotTrades(w http.ResponseWriter, r *http.Request) {
	limit, err := strconv.ParseInt(chi.URLParam(r, "limit"), 0, 64)
	if err != nil || limit == 0 {
		limit = 100
	}
	res, err := db.GetTrades(srv.ctx, limit)
	if err := json.NewEncoder(w).Encode(res); err != nil {
		log.Printf("ERROR: trades encoding failed: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"error": "%v"}`, appErrJsonCreationFailure)
		return
	}
}
