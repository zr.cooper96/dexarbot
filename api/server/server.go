package server

import (
	"log"
	"net/http"

	"gitlab.com/zlyzol/dexarbot/bot"
	"gitlab.com/zlyzol/dexarbot/ctx"
)

type Server struct {
	ctx *ctx.Context
	bot *bot.Bot
	fs  http.Handler
}

func New(ctx *ctx.Context, bot *bot.Bot) *Server {
	srv := Server{
		ctx: ctx,
		bot: bot,
	}
	return &srv
}

func (srv *Server) HandleApiIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Write([]byte("Hello brother!"))
}

func (srv *Server) HandleMainIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	http.ServeFile(w, r, "./../web-ui/index.html")
}

// HandleLive is an http.HandlerFunc that handles liveness checks by
// immediately responding with an HTTP 200 status.
func HandleLive(w http.ResponseWriter, _ *http.Request) {
	writeHealthy(w)
}

// HandleReady is an http.HandlerFunc that handles readiness checks by
// responding with an HTTP 200 status if it is healthy, 500 otherwise.
func (srv *Server) HandleReady(w http.ResponseWriter, r *http.Request) {
	if err := srv.bot.Ping(); err != nil {
		log.Printf("Error: bot ping failed: %v", err)
		writeUnhealthy(w)
		return
	}
	writeHealthy(w)
}

func writeHealthy(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("."))
}

func writeUnhealthy(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte("."))
}
