package server

const (
    appErrDataAccessFailure   = "data access failure"
    appErrJsonCreationFailure = "json creation failure"
)