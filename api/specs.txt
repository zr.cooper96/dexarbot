--------------
API interface (localhost:8080):
"POST", "/api/v1/start" - bot starts scanning orderbooks and doing arbitrage
"POST", "/api/v1//stop" - bot stops -""-
"GET", "/api/v1//status" - gets bot state
"GET", "/api/v1//stats" - gets bot statistics
"GET", "/api/v1//logs" - gets debug logs (there will be a lot of lines)
"GET", "/api/v1//logs/{limit}" - gets last <limit> lines of debug logs
"GET", "/api/v1//trades" - gest info about executed arb trades
"GET", "/api/v1//trades/{limit}" - gets last <limit> arb trades info
--------------
WEB UI:
"http://localhost:8080/" - simple web UI for API calls