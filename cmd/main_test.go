package main

import (
	"fmt"
	"log"
	"math"
	"testing"
	"time"

	//	"time"
	//	"github.com/binance-chain/go-sdk/client/websocket"
	"github.com/binance-chain/go-sdk/client/websocket"

	"gitlab.com/zlyzol/dexarbot/api/router"
	"gitlab.com/zlyzol/dexarbot/api/server"
	"gitlab.com/zlyzol/dexarbot/bot"
	"gitlab.com/zlyzol/dexarbot/bot/order"
	"gitlab.com/zlyzol/dexarbot/bot/orderbook"
	"gitlab.com/zlyzol/dexarbot/ctx"
	"gitlab.com/zlyzol/dexarbot/db"
)
type MockOrderExecutor struct {
	ctx *ctx.Context
}
func (mox MockOrderExecutor) Execute(tAsset, qAsset string, amount, limit float64, orderSide int8) (*order.Order, error) {
	fmt.Print("Mock order Execute")
	o := order.NewOrder(mox.ctx, nil, tAsset, qAsset, amount, limit, orderSide)
	*(o.GetRes()) = order.OrderStatus{Os:3, Pv: order.PriceVol{Price: limit, Amount: amount}}

	return o, nil
}
func (MockOrderExecutor) BurstExecute(orders []*order.Order) error {
	fmt.Print("Mock order BurstExecute")
	return nil
}
///////////////////////////////////////////
/*
func TestDebugGetTrades(t *testing.T){ // used just for debugging
	cx := ctx.New("")
	if cx == nil {
		t.Fatal("failed create context")
	}
	defer cx.Close()
	ord := order.NewOrder(cx, nil, "RUNE-B1A", "BNB", 100, 0.00676887, order.Side.BUY)
	pv := ord.Execute()
	if pv != nil {
		log.Printf("Trade res (%v)\n", pv)
	}
}

func TestDebugMakeOrder(t *testing.T){ // used just for debugging
	cx := ctx.New("")
	if cx == nil {
		t.Fatal("failed create context")
	}
	defer cx.Close()
	w := cx.Dex.GetKeyManager().GetAddr().String()
	err := cx.Dex.SubscribeOrderEvent(w, nil, func(ev []*websocket.OrderEvent) {
		for i, e := range ev {
			log.Printf("ev(%v)=%+v\n\n", i, e)
		}
	}, nil, nil)
	res, err := cx.Dex.CreateOrder("RUNE-B1A", "BUSD-BD1", order.Side.SELL, ctx.F64xI64(0.098127), ctx.F64xI64(529), true)
	if err != nil {
		t.Fatalf("Error: %v", err)
	}
	log.Printf("res=%+v\n\n", res)

	id := res.OrderId
	for i := 0; i < 40; i++ {
		o, err := cx.Dex.GetOrder(id)
		if err != nil {
			log.Printf("Error GetOrder: %v", err)
			time.Sleep(300 * time.Millisecond)
			continue
		}
		time.Sleep(300 * time.Millisecond)
		log.Printf("o(%v)=%s\n%+v\n\n", i, o.Status, o)
	}
	log.Print("END")
}
*/
func TestSubs(t *testing.T){
	ctx := ctx.New("")
	if ctx == nil {
		t.Fatal("failed create context")
	}
	err := ctx.Dex.SubscribeOrderEvent(ctx.GetAcc().Wallet, nil, 
		func(ev []*websocket.OrderEvent) {
			fmt.Printf("Event 1: %v\n", ev)
		}, 
		func(err error) {
			fmt.Printf("Event 1 OnError %v\n", err)	
		}, nil)
	if err != nil {
		t.Fatalf("failed to subscribe %v\n", err)
	}
	
	err = ctx.Dex.SubscribeOrderEvent(ctx.GetAcc().Wallet, nil, 
		func(ev []*websocket.OrderEvent) {
			fmt.Printf("Event 2: %v\n", ev)
		}, 
		func(err error) {
			fmt.Printf("Event 2 OnError %v\n", err)	
		}, nil)
	if err != nil {
		t.Fatalf("failed to subscribe %v\n", err)
	}
	time.Sleep(60*time.Second)
}
func TestMain(t *testing.T) {
	ctx := ctx.New("")
	if ctx == nil {
		t.Fatal("failed create context")
	}
	defer ctx.Close()
	bot := bot.New(ctx)
	if bot == nil {
		t.Fatal("failed create bot")
	}
	defer bot.Stop()
	srv := server.New(ctx, bot)
	if srv == nil {
		t.Fatal("failed create server")
	}
	appRouter := router.New(srv)
	if appRouter == nil {
		t.Fatal("failed create server")
	}
	obRuneUsd, obBnbUsd, obRuneBnb := GetTestOrderbooks()
	obRuneBnb.Bids[0].Price = 0.00790242
	obRuneBnb.Asks[0].Price = 0.00630005
	obRuneBnb.Asks[1].Price = 0.00639898
	bot.DebugOB(obRuneUsd, obBnbUsd, obRuneBnb)
	obRuneUsdMerged := orderbook.Merge(obRuneBnb, obBnbUsd, ctx.TickAndLotTradeBase)
	acc := ctx.GetAcc()
	max := math.Min(ctx.Config.TradeMax, acc.GetBal(ctx.Config.BaseAsset))
	arbRes := orderbook.ChekArbOpportunity(obRuneUsd, obRuneUsdMerged, max)
	if arbRes == nil {
		t.Errorf("Arbitrage failed")
	}
	log.Printf("Wallet balances before arbitrage: %v", acc.GetAllBal())
	exe := &MockOrderExecutor{ctx:ctx}
	dbTrade := bot.ExecuteArbTrade(exe, arbRes)
	log.Printf("Wallet balances after arbitrage: %v", acc.GetAllBal())
	if dbTrade == nil {
		log.Printf("Arbitrage orders execution failed")
		return
	}
	if err := db.InsertTrade(ctx, dbTrade); err != nil {
		log.Printf("failed to write arbitrage results into database: %v", err)
	}
}
func GetTestOrderbooks() (*orderbook.Orderbook, *orderbook.Orderbook, *orderbook.Orderbook) {
	e0 := orderbook.SimpleOrderbookEntry{Price: 0, Amount: 0}
	obRuneUsd := &orderbook.Orderbook{
		Bids: orderbook.OrderbookEntries{
			{orderbook.SimpleOrderbookEntry{Price: 0.119217, Amount: 200.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.118967, Amount: 200.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.116835, Amount: 2177.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.115371, Amount: 200.000000}, e0},
		},
		Asks: orderbook.OrderbookEntries{
			{orderbook.SimpleOrderbookEntry{Price: 0.119301, Amount: 200.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.119302, Amount: 43.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.119318, Amount: 52.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.119720, Amount: 5020.000000}, e0},
		},
	}
	obBnbUsd := &orderbook.Orderbook{
		Bids: orderbook.OrderbookEntries{
			{orderbook.SimpleOrderbookEntry{Price: 17.288200, Amount: 5.264000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 17.229400, Amount: 14.578000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 17.170500, Amount: 5.960000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 17.112200, Amount: 3.925000}, e0},
		},
		Asks: orderbook.OrderbookEntries{
			{orderbook.SimpleOrderbookEntry{Price: 17.440800, Amount: 40.592000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 17.463200, Amount: 17.523000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 17.467800, Amount: 30.418000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 17.490400, Amount: 13.090000}, e0},
		},
	}

	obRuneBnb := &orderbook.Orderbook{
		Bids: orderbook.OrderbookEntries{
			{orderbook.SimpleOrderbookEntry{Price: 0.00670003, Amount: 746.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.00670000, Amount: 2087.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.00665001, Amount: 2000.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.00665000, Amount: 10000.000000}, e0},
		},
		Asks: orderbook.OrderbookEntries{
			{orderbook.SimpleOrderbookEntry{Price: 0.00699896, Amount: 69.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.00699898, Amount: 13724.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.00699900, Amount: 16854.000000}, e0},
			{orderbook.SimpleOrderbookEntry{Price: 0.00704878, Amount: 77548.000000}, e0},
		},
	}
	return obRuneUsd, obBnbUsd, obRuneBnb
}




